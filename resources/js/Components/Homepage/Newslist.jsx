const isNews = (news) => {
    return news.map((data, i) => {
        return (
            <div key={i} className="card lg:w-96 w-full bg-base-100 shadow-xl">
                <figure><img src="https://images.unsplash.com/photo-1673520587276-77478c8f0151?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80" alt="Shoes" /></figure>
                <div className="card-body">
                    <h2 className="card-title">
                        {data.title}
                        {/* <div className="badge badge-secondary">NEW</div> */}
                    </h2>
                    <p>{data.deskripsi}</p>
                    <div className="card-actions justify-end">
                        <div className="badge badge-accent">Kategori : {data.kategori}</div>
                        <div className="badge badge-outline">Author : {data.author}</div>
                    </div>
                </div>
            </div>
        )
    })
}

const noNews = () => {
    return (
        <div>saat ini belum ada berita</div>
    )
}

const Newslist = ({ news }) => {
    return !news ? noNews() : isNews(news)
}

export default Newslist