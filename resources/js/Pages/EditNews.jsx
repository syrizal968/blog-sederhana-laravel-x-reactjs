import React, { useState }  from 'react';
import Navbar from '@/Components/Navbar';
import { Head } from '@inertiajs/react';
import { Inertia } from '@inertiajs/inertia';



export default function EditNews(props) {
    const [title, setTitle] = useState('');
    const [deskripsi, setDeskripsi] = useState('');
    const [kategori, setKategori] = useState('');
    const handleSubmit = () => {
        const data = {
            id: props.myNews.id, title, deskripsi, kategori
        }
        Inertia.post('/news/update', data)
        setTitle('')
        setDeskripsi('')
        setKategori('')
    }
    return (
        <div className='min-h-screen bg-slate-50'>
            <Head title={props.title} />
            <Navbar user={props.auth.user} />
            <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div className="p-4 text-2xl">
                <h1>EDIT BERITA</h1>
            </div>
                    <div className="p-6 text-gray-900">
                        <input type="text" placeholder="Judul" className="input input-bordered w-full m-2" onChange={(title) => setTitle(title.target.value)} defaultValue={props.myNews.title} />
                        <input type="text" placeholder="Deskripsi" className="input input-bordered w-full m-2" onChange={(deskripsi) => setDeskripsi(deskripsi.target.value)} defaultValue={props.myNews.deskripsi} />
                        <input type="text" placeholder="Ktegori" className="input input-bordered w-full m-2" onChange={(katehori) => setKategori(katehori.target.value)} defaultValue={props.myNews.kategori} />
                        <button className='btn btn-primary m-2' onClick={() => handleSubmit()}>UPDATE</button>
                    </div>
                </div>
        </div>
    )
}
