import React  from 'react';
import Navbar from '@/Components/Navbar';
import { Link, Head } from '@inertiajs/react';
import Newslist from '@/Components/Homepage/Newslist';
import Paginator from '@/Components/Homepage/paginator';
import Footer from '@/Components/Homepage/Footer';

export default function Homepage(props) {
    // console.log('props:', props)
    return (
        <div className='min-h-screen bg-slate-50'>
            <Head title={props.title} />
            <Navbar user={props.auth.user}/>
            <div className='flex justify-center flex-col lg:flex-row lg:flex-wrap lg:items-stretch  items-center gap-4 p-4'>
                <Newslist news={props.news.data}/>
            </div>
            <div className='flex justify-center items-center p-4'>
                <Paginator meta={props.news.meta}/>
            </div>
            <div className='flex-1'>
                <Footer />
            </div>
        </div>
        
    )
}