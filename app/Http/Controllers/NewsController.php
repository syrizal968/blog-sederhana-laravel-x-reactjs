<?php

namespace App\Http\Controllers;
use App\Http\Resources\NewsCollection;
use Inertia\Inertia;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = new NewsCollection(News::OrderByDesc('id')->paginate(9));
        return Inertia::render('Homepage', [
        'title' => 'belajar',
        'deskripsi' => 'mari belajar bersama',
        'news' => $news,
    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = new News();
        $news->title = $request->title;
        $news->deskripsi = $request->deskripsi;
        $news->author = auth()->user()->email;
        $news->kategori = $request->kategori;
        $news->save();
        return redirect()->back()->with('message', 'berita berhasil dibuat');
    }

    public function show(News $news)
    {
        $mynews = $news::where('author', auth()->user()->email)->get();
        return Inertia::render('Dashboard', [
        'mynews' => $mynews
    ]);
    }

    public function edit(News  $news, Request $request)
    {
        // dd($request->id);
        return Inertia::render('EditNews', [
            'myNews' => $news->find($request->id)
        ]);
    }

    public function update(Request $request)
    {
        News::where('id', $request->id)->update([
            'title' =>$request->title,
            'deskripsi' =>$request->deskripsi,
            'kategori' =>$request->kategori,
        ]);
        return redirect('news')->with('message', 'berita berhasil diupdate');
    }
    
    public function destroy(Request  $request)
    {
        $news = News::find($request->id);
        $news->delete();
        return redirect('news')->with('message', 'berita berhasil didelete');
    }
}
